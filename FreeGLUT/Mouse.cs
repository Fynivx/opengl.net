using System.Runtime.InteropServices;
using OpenGL.FreeGLUT;

namespace OpenGL.FreeGLUT {
	public class Mouse: Device {
		public static bool Exists {
			get {
				return OpenGL.Native.FreeGLUT.DeviceGet( DeviceGetParam.HasMouse ) == 1;
			}
		}

		public static int ButtonsCount {
			get {
				return OpenGL.Native.FreeGLUT.DeviceGet( DeviceGetParam.NumMouseButtons );
			}
		}

		public CursorImage Cursor {
			get {
				window.SaveCurrentAndUseThis( );
				int result = OpenGL.Native.FreeGLUT.Get( GetSetParam.WindowCursor );
				window.RestorePrevious( );
				return (CursorImage) result;
			}
			set {
				window.SaveCurrentAndUseThis( );
				OpenGL.Native.FreeGLUT.SetCursor( value );
				window.RestorePrevious( );
			}
		}

		public void SetButtonCallback ( MouseButtonCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.MouseFunc( callback );
			window.RestorePrevious( );
		}

		public void SetMotionCallback ( MouseMotionCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.MotionFunc( callback );
			window.RestorePrevious( );
		}

		public void SetPassiveMotionCallback ( MouseMotionCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.PassiveMotionFunc( callback );
			window.RestorePrevious( );
		}

		public void SetEntryCallback ( MouseEntryCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.EntryFunc( callback );
			window.RestorePrevious( );
		}

		public void SetWheelCallback ( MouseWheelCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.MouseWheelFunc( callback );
			window.RestorePrevious( );
		}

		public void WarpPointer ( int x, int y ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.WarpPointer( x, y );
			window.RestorePrevious( );
		}

		internal Mouse ( Window window ): base( window ) {
		}
	}
}
namespace OpenGL.Native {
	internal static partial class FreeGLUT {
		[DllImport( "glut32.dll", EntryPoint="glutSetCursor" )]
		public static extern void SetCursor ( CursorImage cursor );

		[DllImport( "glut32.dll", EntryPoint="glutMouseFunc" )]
		public static extern void MouseFunc ( MouseButtonCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutMotionFunc" )]
		public static extern void MotionFunc ( MouseMotionCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutPassiveMotionFunc" )]
		public static extern void PassiveMotionFunc ( MouseMotionCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutEntryFunc" )]
		public static extern void EntryFunc ( MouseEntryCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutMouseWheelFunc" )]
		public static extern void MouseWheelFunc ( MouseWheelCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutWarpPointer" )]
		public static extern void WarpPointer ( int x, int y );
	}
}
