using System.Runtime.InteropServices;
using OpenGL.FreeGLUT;

namespace OpenGL.FreeGLUT {
	public class Tablet: Device {
		public static bool Exists {
			get {
				return OpenGL.Native.FreeGLUT.DeviceGet( DeviceGetParam.HasTablet ) == 1;
			}
		}

		public static int ButtonsCount {
			get {
				return OpenGL.Native.FreeGLUT.DeviceGet( DeviceGetParam.NumTabletButtons );
			}
		}

		public void SetMotionCallback ( TabletMotionCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.TabletMotionFunc( callback );
			window.RestorePrevious( );
		}

		public void SetButtonCallback ( TabletButtonCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.TabletButtonFunc( callback );
			window.RestorePrevious( );
		}

		internal Tablet ( Window window ): base( window ) {
		}
	}
}
namespace OpenGL.Native {
	internal static partial class FreeGLUT {
		[DllImport( "glut32.dll", EntryPoint="glutTabletMotionFunc" )]
		public static extern void TabletMotionFunc ( TabletMotionCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutTabletButtonFunc" )]
		public static extern void TabletButtonFunc ( TabletButtonCallback callback );
	}
}
