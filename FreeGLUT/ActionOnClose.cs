using System;

namespace OpenGL.FreeGLUT {
	internal enum ActionOnClose: int {
		Exit = 0,
		MainLoopReturnt = 1,
		ContinueExecution = 2,
	}
}

