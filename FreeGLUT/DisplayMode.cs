using System;

namespace OpenGL.FreeGLUT {
	[Flags]
	public enum DisplayMode: uint {
		RGB = 0x0000,
		RGBA = 0x0000,
		Index = 0x0001,
		Single = 0x0000,
		Double = 0x0002,
		Accum = 0x0004,
		Alpha = 0x0008,
		Depth = 0x0010,
		Stencil = 0x0020,
		Multisample = 0x0080,
		Stereo = 0x0100,
		Luminance = 0x0200
	}
}

