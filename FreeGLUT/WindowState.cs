using System;

namespace OpenGL.FreeGLUT {
	public enum WindowState {
		Hidden = 0x0000,
		FullyRetained = 0x0001,
		PartiallyRetained = 0x0002,
		FullyCovered = 0x0003
	}
}

