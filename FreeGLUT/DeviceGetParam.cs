using System;

namespace OpenGL.FreeGLUT {
	internal enum DeviceGetParam {
		HasKeyboard = 0x0258,
		HasMouse = 0x0259,
		HasSpaceball = 0x025A,
		HasDialAndButtonBox = 0x025B,
		HasTablet = 0x025C,
		NumMouseButtons = 0x025D,
		NumSpaceballButtons = 0x025E,
		NumButtonBoxButtons = 0x025F,
		NumDials = 0x0260,
		NumTabletButtons = 0x0261,
		DeviceIgnoreKeyRepeat = 0x0262,
		DeviceKeyRepeat = 0x0263,
		HasJoystick = 0x0264,
		OwnsJoystick = 0x0265,
		JoystickButtons = 0x0266,
		JoystickAxes = 0x0267,
		JoystickPollRate = 0x0268
	}
}

