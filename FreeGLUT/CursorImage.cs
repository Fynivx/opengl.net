using System;

namespace OpenGL.FreeGLUT {
	public enum CursorImage: uint {
		RightArrow = 0x0000,
		LeftArrow = 0x0001,
		Info = 0x0002,
		Destroy = 0x0003,
		Help = 0x0004,
		Cycle = 0x0005,
		Spray = 0x0006,
		Wait = 0x0007,
		Text = 0x0008,
		Crosshair = 0x0009,
		UpDown = 0x000A,
		LeftRight = 0x000B,
		TopSide = 0x000C,
		BottomSide = 0x000D,
		LeftSide = 0x000E,
		RightSide = 0x000F,
		TopLeftCorner = 0x0010,
		TopRightCorner = 0x0011,
		BottomRightCorner = 0x0012,
		BottomLeftCorner = 0x0013,
		Inherit = 0x0064,
		None = 0x0065,
		FullCrosshair = 0x0066
	}
}

