using System.Runtime.InteropServices;
using OpenGL.FreeGLUT;
using OpenGL.Common;

namespace OpenGL.FreeGLUT {
	public class Window: WindowBase {
		public static int DefaultX {
			get {
				return OpenGL.Native.FreeGLUT.Get( GetSetParam.InitWindowX );
			}
		}

		public static int DefaultY {
			get {
				return OpenGL.Native.FreeGLUT.Get( GetSetParam.InitWindowY );
			}
		}

		public static int DefaultWidth {
			get {
				return OpenGL.Native.FreeGLUT.Get( GetSetParam.InitWindowWidth );
			}
		}

		public static int DefaultHeight {
			get {
				return OpenGL.Native.FreeGLUT.Get( GetSetParam.InitWindowHeight );
			}
		}

		public static int ScreenWidth {
			get {
				return OpenGL.Native.FreeGLUT.Get( GetSetParam.ScreenWidth );
			}
		}

		public static int ScreenHeight {
			get {
				return OpenGL.Native.FreeGLUT.Get( GetSetParam.ScreenHeight );
			}
		}

		public static int ScreenWidthMM {
			get {
				return OpenGL.Native.FreeGLUT.Get( GetSetParam.ScreenWidthMM );
			}
		}

		public static int ScreenHeightMM {
			get {
				return OpenGL.Native.FreeGLUT.Get( GetSetParam.ScreenHeightMM );
			}
		}

		public readonly Mouse Mouse;
		public readonly Keyboard Keyboard;
		public readonly Spaceball Spaceball;
		public readonly Tablet Tablet;

		public int X {
			get {
				SaveCurrentAndUseThis( );
				int result = OpenGL.Native.FreeGLUT.Get( GetSetParam.WindowX );
				RestorePrevious( );
				return result;
			}
		}

		public int Y {
			get {
				SaveCurrentAndUseThis( );
				int result = OpenGL.Native.FreeGLUT.Get( GetSetParam.WindowY );
				RestorePrevious( );
				return result;
			}
		}

		public int Width {
			get {
				SaveCurrentAndUseThis( );
				int result = OpenGL.Native.FreeGLUT.Get( GetSetParam.WindowWidth );
				RestorePrevious( );
				return result;
			}
		}

		public int Height {
			get {
				SaveCurrentAndUseThis( );
				int result = OpenGL.Native.FreeGLUT.Get( GetSetParam.WindowHeight );
				RestorePrevious( );
				return result;
			}
		}

		public bool DoubleBuffer {
			get {
				SaveCurrentAndUseThis( );
				int result = OpenGL.Native.FreeGLUT.Get( GetSetParam.WindowDoublebuffer );
				RestorePrevious( );
				return result == 1;
			}
		}

		public bool RGBA {
			get {
				SaveCurrentAndUseThis( );
				int result = OpenGL.Native.FreeGLUT.Get( GetSetParam.WindowRGBA );
				RestorePrevious( );
				return result == 1;
			}
		}

		public bool Stereo {
			get {
				SaveCurrentAndUseThis( );
				int result = OpenGL.Native.FreeGLUT.Get( GetSetParam.WindowStereo );
				RestorePrevious( );
				return result == 1;
			}
		}

		internal override void SaveCurrentAndUseThis ( ) {
			OpenGL.Native.FreeGLUT.PushWindow( );
			OpenGL.Native.FreeGLUT.SetWindow( id );
		}

		internal override void RestorePrevious ( ) {
			OpenGL.Native.FreeGLUT.PopWindow( );
		}

		public static void SetDefaultSize ( int width, int height ) {
			OpenGL.Native.FreeGLUT.InitWindowSize( width, height );
		}

		public static void SetDefaultPosition ( int x, int y ) {
			OpenGL.Native.FreeGLUT.InitWindowPosition( x, y );
		}

		public static void SetDisplayMode ( DisplayMode mode ) {
			OpenGL.Native.FreeGLUT.InitDisplayMode( mode );
		}

		public static void SetIdleCallback ( IdleCallback callback ) {
			OpenGL.Native.FreeGLUT.IdleFunc( callback );
		}

		public static void SetTimerCallback ( uint msecs, TimerCallback callback, ref object value ) {
			OpenGL.Native.FreeGLUT.TimerFunc( msecs, callback, ref value );
		}

		public static void MainLoop ( ) {
			OpenGL.Native.FreeGLUT.MainLoop( ); 
		}

		public static void MainLoopEvent ( ) {
			OpenGL.Native.FreeGLUT.MainLoopEvent( ); 
		}

		public static void LeaveMainLoop ( ) {
			OpenGL.Native.FreeGLUT.LeaveMainLoop( ); 
		}

		public void Use ( ) {
			OpenGL.Native.FreeGLUT.SetWindow( id );
		}

		public void PostRedisplay ( ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.PostRedisplay( );
			RestorePrevious( );
		}

		public void SwapBuffers ( ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.SwapBuffers( );
			RestorePrevious( );
		}

		public void SetPosition ( int x, int y ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.PositionWindow( x, y );
			RestorePrevious( );
		}

		public void SetSize ( int width, int height ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.ReshapeWindow( width, height );
			RestorePrevious( );
		}

		public void FullScreen ( ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.FullScreen( );
			RestorePrevious( );
		}

		public void LeaveFullScreen ( ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.LeaveFullScreen( );
			RestorePrevious( );
		}

		public void FullScreenToggle ( ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.FullScreenToggle( );
			RestorePrevious( );
		}

		public void Show ( ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.ShowWindow( );
			RestorePrevious( );
		}

		public void Hide ( ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.HideWindow( );
			RestorePrevious( );
		}

		public void Iconify ( ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.IconifyWindow( );
			RestorePrevious( );
		}

		public void SetTitle ( string title ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.SetWindowTitle( title );
			RestorePrevious( );
		}

		public void SetIconTitle ( string title ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.SetIconTitle( title );
			RestorePrevious( );
		}

		public void SetDisplayCallback ( DisplayCallback callback ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.DisplayFunc( callback );
			RestorePrevious( );
		}

		public void SetReshapeCallback ( ReshapeCallback callback ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.ReshapeFunc( callback );
			RestorePrevious( );
		}

		public void SetPositionCallback ( PositionCallback callback ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.PositionFunc( callback );
			RestorePrevious( );
		}

		public void SetVisibilityCallback ( VisibilityCallback callback ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.VisibilityFunc( callback );
			RestorePrevious( );
		}

		public void SetStateCallback ( WindowStateCallback callback ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.WindowStateFunc( callback );
			RestorePrevious( );
		}

		public void SetCloseCallback ( CloseCallback callback ) {
			SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.CloseFunc( callback );
			RestorePrevious( );
		}

		public Window ( string name ) {
			uint current = OpenGL.Native.FreeGLUT.GetWindow( );
			id = OpenGL.Native.FreeGLUT.CreateWindow( name );
			this.Mouse = new Mouse ( this );
			this.Keyboard = new Keyboard ( this );
			this.Spaceball = new Spaceball ( this );

			if ( current != 0 )
				OpenGL.Native.FreeGLUT.SetWindow( current );
		}

		~Window ( ) {
			OpenGL.Native.FreeGLUT.DestroyWindow( id );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class FreeGLUT {
		[DllImport( "glut32.dll", EntryPoint="glutInitWindowSize" )]
		public static extern void InitWindowSize ( int width, int height );

		[DllImport( "glut32.dll", EntryPoint="glutInitWindowPosition" )]
		public static extern void InitWindowPosition ( int x, int y );

		[DllImport( "glut32.dll", EntryPoint="glutInitDisplayMode" )]
		public static extern void InitDisplayMode ( DisplayMode mode );

		[DllImport( "glut32.dll", EntryPoint="glutMainLoop" )]
		public static extern void MainLoop ( );

		[DllImport( "glut32.dll", EntryPoint="glutMainLoopEvent" )]
		public static extern void MainLoopEvent ( );

		[DllImport( "glut32.dll", EntryPoint="glutLeaveMainLoop" )]
		public static extern void LeaveMainLoop ( );

		[DllImport( "glut32.dll", EntryPoint="glutCreateWindow" )]
		public static extern uint CreateWindow ( string name );

		[DllImport( "glut32.dll", EntryPoint="glutSetWindow" )]
		public static extern void SetWindow ( uint id );

		[DllImport( "glut32.dll", EntryPoint="glutGetWindow" )]
		public static extern uint GetWindow ( );

		[DllImport( "glut32.dll", EntryPoint="glutDestroyWindow" )]
		public static extern void DestroyWindow ( uint id );

		[DllImport( "glut32.dll", EntryPoint="glutPostRedisplay" )]
		public static extern void PostRedisplay ( );

		[DllImport( "glut32.dll", EntryPoint="glutSwapBuffers" )]
		public static extern void SwapBuffers ( );

		[DllImport( "glut32.dll", EntryPoint="glutPositionWindow" )]
		public static extern void PositionWindow ( int x, int y );

		[DllImport( "glut32.dll", EntryPoint="glutReshapeWindow" )]
		public static extern void ReshapeWindow ( int width, int height );

		[DllImport( "glut32.dll", EntryPoint="glutFullScreen" )]
		public static extern void FullScreen ( );

		[DllImport( "glut32.dll", EntryPoint="glutLeaveFullScreen" )]
		public static extern void LeaveFullScreen ( );

		[DllImport( "glut32.dll", EntryPoint="glutFullScreenToggle" )]
		public static extern void FullScreenToggle ( );

		[DllImport( "glut32.dll", EntryPoint="glutPopWindow" )]
		public static extern void PopWindow ( );

		[DllImport( "glut32.dll", EntryPoint="glutPushWindow" )]
		public static extern void PushWindow ( );

		[DllImport( "glut32.dll", EntryPoint="glutShowWindow" )]
		public static extern void ShowWindow ( );

		[DllImport( "glut32.dll", EntryPoint="glutHideWindow" )]
		public static extern void HideWindow ( );

		[DllImport( "glut32.dll", EntryPoint="glutIconifyWindow" )]
		public static extern void IconifyWindow ( );

		[DllImport( "glut32.dll", EntryPoint="glutSetWindowTitle" )]
		public static extern void SetWindowTitle ( string title );

		[DllImport( "glut32.dll", EntryPoint="glutSetIconTitle" )]
		public static extern void SetIconTitle ( string title );

		[DllImport( "glut32.dll", EntryPoint="glutDisplayFunc" )]
		public static extern void DisplayFunc ( DisplayCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutReshapeFunc" )]
		public static extern void ReshapeFunc ( ReshapeCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutPositionFunc" )]
		public static extern void PositionFunc ( PositionCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutVisibilityFunc" )]
		public static extern void VisibilityFunc ( VisibilityCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutWindowStateFunc" )]
		public static extern void WindowStateFunc ( WindowStateCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutCloseFunc" )]
		public static extern void CloseFunc ( CloseCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutIdleFunc" )]
		public static extern void IdleFunc ( IdleCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutTimerFunc" )]
		public static extern void TimerFunc ( uint msecs, TimerCallback callback, ref object value );

		[DllImport( "glut32.dll", EntryPoint="glutGet" )]
		public static extern int Get ( GetSetParam param );
	}
}
