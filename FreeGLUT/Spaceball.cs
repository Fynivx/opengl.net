using System.Runtime.InteropServices;
using OpenGL.FreeGLUT;

namespace OpenGL.FreeGLUT {
	public class Spaceball: Device {
		public static bool Exists {
			get {
				return OpenGL.Native.FreeGLUT.DeviceGet( DeviceGetParam.HasSpaceball ) == 1;
			}
		}

		public static int ButtonsCount {
			get {
				return OpenGL.Native.FreeGLUT.DeviceGet( DeviceGetParam.NumSpaceballButtons );
			}
		}

		public void SetMotionCallback ( SpaceballMotionCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.SpaceballMotionFunc( callback );
			window.RestorePrevious( );
		}

		public void SetRotateCallback ( SpaceballRotateCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.SpaceballRotateFunc( callback );
			window.RestorePrevious( );
		}

		public void SetButtonCallback ( SpaceballButtonCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.SpaceballButtonFunc( callback );
			window.RestorePrevious( );
		}

		internal Spaceball ( Window window ): base( window ) {
		}
	}
}
namespace OpenGL.Native {
	internal static partial class FreeGLUT {
		[DllImport( "glut32.dll", EntryPoint="glutSpaceballMotionFunc" )]
		public static extern void SpaceballMotionFunc ( SpaceballMotionCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutSpaceballRotateFunc" )]
		public static extern void SpaceballRotateFunc ( SpaceballRotateCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutSpaceballButtonFunc" )]
		public static extern void SpaceballButtonFunc ( SpaceballButtonCallback callback );
	}
}
