namespace OpenGL.FreeGLUT {
	public enum MouseEntryState: uint {
		Entered = 1,
		Left = 0
	}
}