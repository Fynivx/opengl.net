using System.Runtime.InteropServices;
using OpenGL.FreeGLUT;

namespace OpenGL.FreeGLUT {
	public abstract class Device {
		protected Window window;

		internal Device ( Window window ) {
			this.window = window;
		}
	}
}
namespace OpenGL.Native {
	internal static partial class FreeGLUT {
		[DllImport( "glut32.dll", EntryPoint="glutDeviceGet" )]
		public static extern int DeviceGet ( DeviceGetParam param );
	}
}
