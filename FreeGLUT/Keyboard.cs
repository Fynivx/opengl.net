using System.Runtime.InteropServices;
using OpenGL.FreeGLUT;

namespace OpenGL.FreeGLUT {
	public class Keyboard: Device {
		public static bool Exists {
			get {
				return OpenGL.Native.FreeGLUT.DeviceGet( DeviceGetParam.HasKeyboard ) == 1;
			}
		}

		public void SetKeyCallback ( KeyCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.KeyboardFunc( callback );
			window.RestorePrevious( );
		}

		public void SetSpecialKeyCallback ( SpecialKeyCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.SpecialFunc( callback );
			window.RestorePrevious( );
		}

		public void SetKeyUpCallback ( KeyCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.KeyboardUpFunc( callback );
			window.RestorePrevious( );
		}

		public void SetSpecialKeyUpCallback ( SpecialKeyCallback callback ) {
			window.SaveCurrentAndUseThis( );
			OpenGL.Native.FreeGLUT.SpecialUpFunc( callback );
			window.RestorePrevious( );
		}

		internal Keyboard ( Window window ): base( window ) {
		}
	}
}
namespace OpenGL.Native {
	internal static partial class FreeGLUT {
		[DllImport( "glut32.dll", EntryPoint="glutKeyboardFunc" )]
		public static extern void KeyboardFunc ( KeyCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutSpecialFunc" )]
		public static extern void SpecialFunc ( SpecialKeyCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutKeyboardUpFunc" )]
		public static extern void KeyboardUpFunc ( KeyCallback callback );

		[DllImport( "glut32.dll", EntryPoint="glutSpecialUpFunc" )]
		public static extern void SpecialUpFunc ( SpecialKeyCallback callback );
	}
}