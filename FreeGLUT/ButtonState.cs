namespace OpenGL.FreeGLUT {
	public enum ButtonState: uint {
		Up = 1,
		Down = 0
	}
}