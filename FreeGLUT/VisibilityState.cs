namespace OpenGL.FreeGLUT {
	public enum VisibilityState: uint {
		Visible = 1,
		NotVisible = 0
	}
}