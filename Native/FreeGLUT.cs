using System.Runtime.InteropServices;
using System;
using OpenGL.FreeGLUT;

namespace OpenGL.Native {
	internal static partial class FreeGLUT {
		static FreeGLUT ( ) {
			string [] argv = Environment.GetCommandLineArgs( );
			int argc = argv.Length;
			Init( ref argc, argv );
			SetOption( GetSetParam.ActionOnWindowClose, (int) ActionOnClose.ContinueExecution );
		}

		[DllImport( "glut32.dll", EntryPoint = "glutInit" )]
		public static extern void Init ( ref int argc, string [] argv );

		[DllImport( "glut32.dll", EntryPoint = "glutSetOption" )]
		public static extern void SetOption ( GetSetParam option, int value );
	}
}
