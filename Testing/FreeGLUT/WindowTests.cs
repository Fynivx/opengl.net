﻿using System;
using System.Linq;
using NUnit.Framework;
using OpenGL.FreeGLUT;

namespace Testing.FreeGLUT
{
	[TestFixture]
	public class WindowTests
	{
		[Test]
		// Demonstrates calling unmanaged code with parameter.
		public void ShouldShowWindow ()
		{
			var window = new Window ("This Window Should Be Shown");

			window.Show ();

			System.Threading.Thread.Sleep (TimeSpan.FromSeconds (3));
		}

		[Test]
		// Demonstrates using of already created unmanaged object.
		public void ShouldRenameWindowFewTime ()
		{
			var window = new Window ("This Window Should Be Renamed");

			window.Show ();

			foreach (var i in Enumerable.Range(1, 30))
			{
				System.Threading.Thread.Sleep (TimeSpan.FromSeconds (0.1));
				window.SetTitle("Renamed: iteration " + i);
			}
		}

		[Test]
		// Demonstrates calling managed code from unmanaged and throwing an exception through.
		public void ShouldFailOnClick ()
		{
			var window = new Window ("Do Not Touch This!");

			window.SetDisplayCallback (() => {}); // FreeGLUT can't run event loop without display callback;

			window.Mouse.SetButtonCallback ((btn, state, x, y) => {
				Window.LeaveMainLoop();
				throw new InvalidOperationException("Oh my! You made the mouse button #" + btn + " " + state + "! I told you - do not touch.");
			});

			window.Show();

			Assert.Throws(typeof(InvalidOperationException), Window.MainLoop); // running window main loop, event handling works inside of it.
		}
	}
}

