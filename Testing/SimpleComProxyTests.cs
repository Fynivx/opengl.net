﻿using System;
using System.Runtime.InteropServices;

namespace Testing
{
	public class SimpleComProxyTests
	{
		public void ShouldShowCOMWindow ()
		{
			var guid = new Guid ("52d78e0c-536d-42b2-ae0d-57552974f0d4");
			var type = Type.GetTypeFromCLSID (guid);
			var instance = Activator.CreateInstance (type);

			var method = type.GetMethod ("Show");

			method.Invoke (instance, new object[0]);

			System.Threading.Thread.Sleep (TimeSpan.FromSeconds (3));
		}
	}
}

