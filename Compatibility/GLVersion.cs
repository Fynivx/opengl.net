using System;

namespace OpenGL.Compatibility {
	public class GLVersion {
		public readonly uint Major;
		public readonly uint Minor;

		public GLVersion ( uint major, uint minor ) {
			Major = major;
			Minor = minor;
		}
	}
}

