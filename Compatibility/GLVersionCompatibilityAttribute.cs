using System;

namespace OpenGL.Compatibility {
	[AttributeUsage(
		AttributeTargets.Class |
		AttributeTargets.Method |
		AttributeTargets.Property
	)]
	public class GLVersionCompatibilityAttribute: Attribute {
		public GLVersion Version;

		public GLVersionCompatibilityAttribute ( uint major, uint minor ) {
			Version = new GLVersion ( major, minor );
		}
	}
}

