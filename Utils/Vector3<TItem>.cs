namespace OpenGL.Utils {
	public struct Vector3<TItem> {
		public TItem X;
		public TItem Y;
		public TItem Z;

		public Vector3 ( TItem x, TItem y, TItem z ) {
			X = x;
			Y = y;
			Z = z;
		}
	}
}

