using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform2ui: Uniform {
		internal Uniform2ui ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( uint val1, uint val2 ) {
			GL.ProgramUniform2ui( Program.id, id, val1, val2 );
		}

		public void Set ( uint [] values ) {
			GL.ProgramUniform2uiv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform2ui" )]
		public static extern uint ProgramUniform2ui ( uint program, uint uniform, uint val1, uint val2 );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform2uiv" )]
		public static extern uint ProgramUniform2uiv ( uint program, uint uniform, int count, uint [] values );
	}
}