using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class UniformMatrix4x3f: Uniform {
		internal UniformMatrix4x3f ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( float [] values, bool transpose ) {
			GL.ProgramUniformMatrix4x3fv( Program.id, id, values.Length, transpose, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniformMatrix4x3fv" )]
		public static extern uint ProgramUniformMatrix4x3fv ( uint program, uint uniform, int count, bool transpose, float [] values );
	}
}