using OpenGL.Compatibility;

namespace OpenGL.Shading {
	[GLVersionCompatibility(4,3)]
	public class ComputeShader:Shader {
		public ComputeShader ( params string [] sources )
		:base ( ShaderType.Compute, sources ) {
		}
	}
}
