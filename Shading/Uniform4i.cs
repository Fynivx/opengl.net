using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform4i: Uniform {
		internal Uniform4i ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( int val1, int val2, int val3, int val4 ) {
			GL.ProgramUniform4i( Program.id, id, val1, val2, val3, val4 );
		}

		public void Set ( int [] values ) {
			GL.ProgramUniform4iv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform4i" )]
		public static extern uint ProgramUniform4i ( uint program, uint uniform, int val1, int val2, int val3, int val4 );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform4iv" )]
		public static extern uint ProgramUniform4iv ( uint program, uint uniform, int count, int [] values );
	}
}