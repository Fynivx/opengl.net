using System.Runtime.InteropServices;
using System.Collections.ObjectModel;
using System.Text;
using OpenGL.Common;
using OpenGL.Native;
using OpenGL.Shading;

namespace OpenGL.Shading {
	public class ShaderProgram: BaseResource {

		private Shader [] shaders;
		public ReadOnlyCollection<Shader> Shaders;

		public ShaderProgram ( params Shader [] shaders ) {
			id = GL.CreateProgram( );
			this.shaders = shaders;

			foreach ( Shader shader in this.shaders )
				GL.AttachShader( id, shader.id );

			GL.LinkProgram( id );

			int status;

			GL.GetProgramiv( id, GL.GetProgramiv1Param.LinkStatus, out status );

			if ( status == 0 ) {
				int info_length;

				GL.GetProgramiv( id, GL.GetProgramiv1Param.InfoLogLength, out info_length );

				byte [] buffer = new byte[info_length];
				GL.GetProgramInfoLog( id, info_length, ref info_length, buffer );

				GL.DeleteProgram( id );

				throw new LinkFailedException ( Encoding.ASCII.GetString( buffer ) );
			}
		}

		public void Use ( ) {
			GL.UseProgram( id );
		}

		public Uniform1f GetUniform1f ( string name ) {
			return new Uniform1f ( this, name );
		}

		public Uniform2f GetUniform2f ( string name ) {
			return new Uniform2f ( this, name );
		}

		public Uniform3f GetUniform3f ( string name ) {
			return new Uniform3f ( this, name );
		}

		public Uniform4f GetUniform4f ( string name ) {
			return new Uniform4f ( this, name );
		}

		public Uniform1i GetUniform1i ( string name ) {
			return new Uniform1i ( this, name );
		}

		public Uniform2i GetUniform2i ( string name ) {
			return new Uniform2i ( this, name );
		}

		public Uniform3i GetUniform3i ( string name ) {
			return new Uniform3i ( this, name );
		}

		public Uniform4i GetUniform4i ( string name ) {
			return new Uniform4i ( this, name );
		}

		public Uniform1ui GetUniform1ui ( string name ) {
			return new Uniform1ui ( this, name );
		}

		public Uniform2ui GetUniform2ui ( string name ) {
			return new Uniform2ui ( this, name );
		}

		public Uniform3ui GetUniform3ui ( string name ) {
			return new Uniform3ui ( this, name );
		}

		public Uniform4ui GetUniform4ui ( string name ) {
			return new Uniform4ui ( this, name );
		}

		~ShaderProgram ( ) {
			GL.DeleteProgram( id );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		public enum GetProgramiv1Param: uint {
			DeleteStatus = 0x8B80,
			LinkStatus = 0x8B82,
			ValidateStatus = 0x8B83,
			InfoLogLength = 0x8B84,
			AttachedShaders = 0x8B85,
			ActiveAtomicCounterBuffers,
			ActiveAttributes = 0x8B89,
			ActiveAttributeMaxLength = 0x8B8A,
			ActiveUniforms = 0x8B86,
			ActiveUniformBlocks,
			ActiveUniformBlockMaxNameLength,
			ActiveUniformMaxLength = 0x8B87,
			ProgramBinaryLength,
			TransformFeedbackBufferMode,
			TransformFeedbackVaryings,
			TransformFeedbackVaryingMaxLength,
			GeometryVerticesOut,
			GeometryInputType,
			GeometryOutputType
		}

		public enum GetProgramivParam: uint {
			ComputeWorkGroupSize
		}

		[DllImport( "opengl32.dll", EntryPoint = "glCreateProgram" )]
		public static extern uint CreateProgram ( );

		[DllImport( "opengl32.dll", EntryPoint = "glAttachShader" )]
		public static extern void AttachShader ( uint program, uint shader );

		[DllImport( "opengl32.dll", EntryPoint = "glDetachShader" )]
		public static extern void DetachShader ( uint program, uint shader );

		[DllImport( "opengl32.dll", EntryPoint = "glLinkProgram" )]
		public static extern void LinkProgram ( uint program );

		[DllImport( "opengl32.dll", EntryPoint = "glUseProgram" )]
		public static extern void UseProgram ( uint program );

		[DllImport( "opengl32.dll", EntryPoint = "glValidateProgram" )]
		public static extern void ValidateProgram ( uint program );

		[DllImport( "opengl32.dll", EntryPoint = "glDeleteProgram" )]
		public static extern void DeleteProgram ( uint program );

		[DllImport( "opengl32.dll", EntryPoint = "glGetProgramiv" )]
		public static extern void GetProgramiv ( uint program, GetProgramiv1Param param, out int value );

		[DllImport( "opengl32.dll", EntryPoint = "glGetProgramiv" )]
		public static extern void GetProgramiv ( uint program, GetProgramivParam param, int [] values );

		[DllImport( "opengl32.dll", EntryPoint = "glGetProgramInfoLog" )]
		public static extern void GetProgramInfoLog ( uint program, int maxLength, ref int length, byte [] result );
	}
}