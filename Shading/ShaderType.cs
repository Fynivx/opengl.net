namespace OpenGL.Shading {
	internal enum ShaderType: uint {
		Fragment = 0x8B30,
		Vertex = 0x8B31,
		Geometry = 0x8DD9,
		TessEvaluation = 0x8E87,
		TessControl = 0x8E88,
		Compute = 0x91B9
	}
}