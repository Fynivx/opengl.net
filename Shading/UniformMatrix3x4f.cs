using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class UniformMatrix3x4f: Uniform {
		internal UniformMatrix3x4f ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( float [] values, bool transpose ) {
			GL.ProgramUniformMatrix3x4fv( Program.id, id, values.Length, transpose, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniformMatrix3x4fv" )]
		public static extern uint ProgramUniformMatrix3x4fv ( uint program, uint uniform, int count, bool transpose, float [] values );
	}
}