using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform1i: Uniform {
		internal Uniform1i ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( int value ) {
			GL.ProgramUniform1i( Program.id, id, value );
		}

		public void Set ( int [] values ) {
			GL.ProgramUniform1iv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform1i" )]
		public static extern uint ProgramUniform1i ( uint program, uint uniform, int value );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform1iv" )]
		public static extern uint ProgramUniform1iv ( uint program, uint uniform, int count, int [] value );
	}
}