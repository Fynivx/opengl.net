using System.Runtime.InteropServices;
using System.Text;
using OpenGL.Common;
using OpenGL.Native;
using OpenGL.Shading;

namespace OpenGL.Shading {
	public class Shader: BaseResource {

		private int sourceLength;

		public int SourceLength {
			get {
				GL.GetShaderiv( id, GL.GetShaderivParam.SorceLength, out sourceLength );
				return sourceLength;
			}
		}

		internal Shader ( ShaderType type, params string [] sources ) {
			id = GL.CreateShader( type );
			int [] length = new int[sources.Length];

			for ( int i = 0; i < sources.Length; i++ )
				length [i] = sources [i].Length;

			GL.ShaderSource( id, sources.Length, sources, length );

			GL.CompileShader( id );

			int status;

			GL.GetShaderiv( id, GL.GetShaderivParam.CompileStatus, out status );

			if ( status == 0 ) {
				int info_length;

				GL.GetShaderiv( id, GL.GetShaderivParam.InfoLogLength, out info_length );

				byte [] buffer = new byte[info_length];

				GL.GetShaderInfoLog( id, info_length, ref info_length, buffer );

				GL.DeleteShader( id );

				throw new CompilationFailedException ( Encoding.ASCII.GetString( buffer ) );
			}
		}

		~Shader () {
			GL.DeleteShader( id );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		public enum GetShaderivParam: uint {
			ShaderType = 0x8B4F,
			DeleteStatus = 0x8B80,
			CompileStatus = 0x8B81,
			InfoLogLength = 0x8B84,
			SorceLength = 0x8B88
		}

		[DllImport( "opengl32.dll", EntryPoint = "glCreateShader" )]
		public static extern uint CreateShader ( ShaderType type );

		[DllImport( "opengl32.dll", EntryPoint = "glDeleteShader" )]
		public static extern void DeleteShader ( uint shader );

		[DllImport( "opengl32.dll", EntryPoint = "glShaderSource" )]
		public static extern void ShaderSource ( uint shader, int count, string [] source, int [] length );

		[DllImport( "opengl32.dll", EntryPoint = "glCompileShader" )]
		public static extern void CompileShader ( uint shader );

		[DllImport( "opengl32.dll", EntryPoint = "glGetShaderiv" )]
		public static extern void GetShaderiv ( uint shader, GetShaderivParam param, out int values );

		[DllImport( "opengl32.dll", EntryPoint = "glGetShaderInfoLog" )]
		public static extern void GetShaderInfoLog ( uint shader, int max_length, ref int length, byte [] result );
	}
}