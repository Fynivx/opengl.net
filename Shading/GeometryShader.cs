using OpenGL.Compatibility;

namespace OpenGL.Shading {
	[GLVersionCompatibility(3,2)]
	public class GeometryShader: Shader {
		public GeometryShader ( params string [] sources )
		:base( ShaderType.Geometry, sources ) {
		}
	}
}

