using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform3ui: Uniform {
		internal Uniform3ui ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( uint val1, uint val2, uint val3 ) {
			GL.ProgramUniform3ui( Program.id, id, val1, val2, val3 );
		}

		public void Set ( uint [] values ) {
			GL.ProgramUniform3uiv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform3ui" )]
		public static extern uint ProgramUniform3ui ( uint program, uint uniform, uint val1, uint val2, uint val3 );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform3uiv" )]
		public static extern uint ProgramUniform3uiv ( uint program, uint uniform, int count, uint [] values );
	}
}