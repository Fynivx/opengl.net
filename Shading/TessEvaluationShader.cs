using OpenGL.Compatibility;

namespace OpenGL.Shading {
	[GLVersionCompatibility(4,0)]
	public class TessEvaluationShader:Shader {
		public TessEvaluationShader ( params string [] sources )
		:base ( ShaderType.TessEvaluation, sources ) {
		}
	}
}
