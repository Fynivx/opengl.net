using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform3f: Uniform {
		internal Uniform3f ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( float val1, float val2, float val3 ) {
			GL.ProgramUniform3f( Program.id, id, val1, val2, val3 );
		}

		public void Set ( float [] values ) {
			GL.ProgramUniform3fv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform3f" )]
		public static extern uint ProgramUniform3f ( uint program, uint uniform, float val1, float val2, float val3 );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform3fv" )]
		public static extern uint ProgramUniform3fv ( uint program, uint uniform, int count, float [] values );
	}
}