using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform4f: Uniform {
		internal Uniform4f ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( float val1, float val2, float val3, float val4 ) {
			GL.ProgramUniform4f( Program.id, id, val1, val2, val3, val4 );
		}

		public void Set ( float [] values ) {
			GL.ProgramUniform4fv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform4f" )]
		public static extern uint ProgramUniform4f ( uint program, uint uniform, float val1, float val2, float val3, float val4 );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform4fv" )]
		public static extern uint ProgramUniform4fv ( uint program, uint uniform, int count, float [] values );
	}
}