using OpenGL.Compatibility;

namespace OpenGL.Shading {
	[GLVersionCompatibility(3,0)]
	public class VertexShader: Shader {
		public VertexShader ( params string [] sources )
			:base( ShaderType.Vertex, sources ) {
		}
	}
}

