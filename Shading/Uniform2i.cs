using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform2i: Uniform {
		internal Uniform2i ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( int val1, int val2 ) {
			GL.ProgramUniform2i( Program.id, id, val1, val2 );
		}

		public void Set ( int [] values ) {
			GL.ProgramUniform2iv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform2i" )]
		public static extern uint ProgramUniform2i ( uint program, uint uniform, int val1, int val2 );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform2iv" )]
		public static extern uint ProgramUniform2iv ( uint program, uint uniform, int count, int [] values );
	}
}