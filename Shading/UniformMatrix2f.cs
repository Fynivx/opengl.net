using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class UniformMatrix2f: Uniform {
		internal UniformMatrix2f ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( float [] values, bool transpose ) {
			GL.ProgramUniformMatrix2fv( Program.id, id, values.Length, transpose, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniformMatrix2fv" )]
		public static extern uint ProgramUniformMatrix2fv ( uint program, uint uniform, int count, bool transpose, float [] values );
	}
}