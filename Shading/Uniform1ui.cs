using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform1ui: Uniform {
		internal Uniform1ui ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( uint value ) {
			GL.ProgramUniform1ui( Program.id, id, value );
		}

		public void Set ( uint [] values ) {
			GL.ProgramUniform1uiv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform1ui" )]
		public static extern uint ProgramUniform1ui ( uint program, uint uniform, uint value );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform1uiv" )]
		public static extern uint ProgramUniform1uiv ( uint program, uint uniform, int count, uint [] value );
	}
}