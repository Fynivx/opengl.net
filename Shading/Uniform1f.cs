using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform1f: Uniform {
		internal Uniform1f ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( float value ) {
			GL.ProgramUniform1f( Program.id, id, value );
		}

		public void Set ( float [] values ) {
			GL.ProgramUniform1fv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform1f" )]
		public static extern uint ProgramUniform1f ( uint program, uint uniform, float value );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform1fv" )]
		public static extern uint ProgramUniform1fv ( uint program, uint uniform, int count, float [] value );
	}
}