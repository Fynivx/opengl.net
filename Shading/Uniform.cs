using System.Runtime.InteropServices;
using OpenGL.Common;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform: BaseResource {

		public readonly ShaderProgram Program;

		protected internal Uniform ( ShaderProgram program, string name ) {
			Program = program;
			id = GL.GetUniformLocation( program.id, name );
		}

		~Uniform () {
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glGetUniformLocation" )]
		public static extern uint GetUniformLocation ( uint program, string name );
	}
}