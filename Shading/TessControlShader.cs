using OpenGL.Compatibility;

namespace OpenGL.Shading {
	[GLVersionCompatibility(4,0)]
	public class TessControlShader:Shader {
		public TessControlShader ( params string [] sources )
		:base ( ShaderType.TessControl, sources ) {
		}
	}
}