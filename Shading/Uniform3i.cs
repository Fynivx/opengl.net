using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform3i: Uniform {
		internal Uniform3i ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( int val1, int val2, int val3 ) {
			GL.ProgramUniform3i( Program.id, id, val1, val2, val3 );
		}

		public void Set ( int [] values ) {
			GL.ProgramUniform3iv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform3i" )]
		public static extern uint ProgramUniform3i ( uint program, uint uniform, int val1, int val2, int val3 );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform3iv" )]
		public static extern uint ProgramUniform3iv ( uint program, uint uniform, int count, int [] values );
	}
}