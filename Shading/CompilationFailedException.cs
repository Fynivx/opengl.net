using System;

namespace OpenGL.Shading {
	public class CompilationFailedException: Exception {
		public CompilationFailedException ( string message )
			:base ( message ) { }
	}
}

