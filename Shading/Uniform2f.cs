using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform2f: Uniform {
		internal Uniform2f ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( float val1, float val2 ) {
			GL.ProgramUniform2f( Program.id, id, val1, val2 );
		}

		public void Set ( float [] values ) {
			GL.ProgramUniform2fv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform2f" )]
		public static extern uint ProgramUniform2f ( uint program, uint uniform, float val1, float val2 );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform2fv" )]
		public static extern uint ProgramUniform2fv ( uint program, uint uniform, int count, float [] values );
	}
}