using System;

namespace OpenGL.Shading {
	public class LinkFailedException: Exception {
		public LinkFailedException ( string message )
			:base( message ) { }
	}
}

