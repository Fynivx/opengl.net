using System;

namespace OpenGL.Shading {
	public class AttribNotFoundException: ArgumentException {
		public AttribNotFoundException ( string attrib_name )
			:base( "Attribute " + attrib_name + " not found." ) {
		}
	}
}

