using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class UniformMatrix2x4f: Uniform {
		internal UniformMatrix2x4f ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( float [] values, bool transpose ) {
			GL.ProgramUniformMatrix2x4fv( Program.id, id, values.Length, transpose, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniformMatrix2x4fv" )]
		public static extern uint ProgramUniformMatrix2x4fv ( uint program, uint uniform, int count, bool transpose, float [] values );
	}
}