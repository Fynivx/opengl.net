using System.Runtime.InteropServices;
using OpenGL.Native;

namespace OpenGL.Shading {
	public class Uniform4ui: Uniform {
		internal Uniform4ui ( ShaderProgram program, string name )
			:base( program, name ) {
		}

		public void Set ( uint val1, uint val2, uint val3, uint val4 ) {
			GL.ProgramUniform4ui( Program.id, id, val1, val2, val3, val4 );
		}

		public void Set ( uint [] values ) {
			GL.ProgramUniform4uiv( Program.id, id, values.Length, values );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform4ui" )]
		public static extern uint ProgramUniform4ui ( uint program, uint uniform, uint val1, uint val2, uint val3, uint val4 );

		[DllImport( "opengl32.dll", EntryPoint = "glProgramUniform4uiv" )]
		public static extern uint ProgramUniform4uiv ( uint program, uint uniform, int count, uint [] values );
	}
}