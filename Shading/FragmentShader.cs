using OpenGL.Compatibility;

namespace OpenGL.Shading {
	[GLVersionCompatibility(3,0)]
	public class FragmentShader: Shader {
		public FragmentShader ( params string [] sources )
		:base( ShaderType.Fragment, sources ) {
		}
	}
}

