﻿using System;
using System.Runtime.InteropServices;

namespace SimpleCOMProxy
{
	[ComVisible(true)]
	[Guid("68afb93e-3ab1-4818-9710-b078297f85fe")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IWindow
	{
		void Show();
	}
}