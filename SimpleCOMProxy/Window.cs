﻿using System;
using System.Runtime.InteropServices;

namespace SimpleCOMProxy
{
	[ComVisible(true)]
	[Guid("52d78e0c-536d-42b2-ae0d-57552974f0d4")]
	[ClassInterface(ClassInterfaceType.None)]
	public class Window : OpenGL.FreeGLUT.Window, IWindow
	{
		public Window ()
			: base ("COM Window")
		{
		}

		void IWindow.Show ()
		{
			base.Show ();
		}
	}
}