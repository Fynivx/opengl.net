using OpenGL.Compatibility;

namespace OpenGL.Buffering {
	[GLVersionCompatibilityAttribute( 3,1 )]
	public class UniformBuffer<TItem>: Buffer<TItem> where TItem: struct {
		public UniformBuffer ( BufferUsage usage )
		:base( BufferType.UniformBuffer, usage ) {
		}
	}
}