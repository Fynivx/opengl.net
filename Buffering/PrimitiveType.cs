using System;

namespace OpenGL {
	public enum PrimitiveType: uint {
		Points					= 0x0000,
		Lines					= 0x0001,
		LineLoop				= 0x0002,
		LineStrip				= 0x0003,
		Triangles				= 0x0004,
		TriangleStrip			= 0x0005,
		TriangleFan				= 0x0006,
		Quads					= 0x0007,
		QuadStrip				= 0x0008,
		Polygon					= 0x0009,

		// With geometry shaders only:

		LinesAdjacency			= 0x000A,
		LineStripAdjacency		= 0x000B,
		TrianglesAdjacency		= 0x000C,
		TriangleStripAdjacency	= 0x000D
	}
}

