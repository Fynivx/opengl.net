using System.Runtime.InteropServices;
using OpenGL.Common;
using OpenGL.Native;
using OpenGL.Buffering;

namespace OpenGL.Buffering {
	public class Framebuffer: BaseResource {
		protected readonly FramebufferType Type;

		public Framebuffer ( FramebufferType type ) {
			GL.GenFramebuffers( 1, out id );
		}

		public void Bind ( ) {
			GL.BindFramebuffer( Type, id );
		}

		~Framebuffer ( ) {
			GL.DeleteFramebuffers( 1, ref id );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glGenFramebuffers" )]
		public static extern void GenFramebuffers ( int count, out uint buffer );

		[DllImport( "opengl32.dll", EntryPoint = "glBindFramebuffer" )]
		public static extern void BindFramebuffer ( FramebufferType type, uint buffer );

		[DllImport( "opengl32.dll", EntryPoint = "glDeleteFramebuffers" )]
		public static extern void DeleteFramebuffers ( int count, ref uint buffer );
	}
}