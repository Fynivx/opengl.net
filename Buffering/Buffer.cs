using System.Runtime.InteropServices;
using OpenGL.Common;
using OpenGL.Native;
using OpenGL.Buffering;

namespace OpenGL.Buffering {
	public class Buffer: BaseResource {
		protected readonly BufferType Type;
		protected readonly BufferUsage Usage;

		public Buffer ( BufferType type, BufferUsage usage ) {
			GL.GenBuffers( 1, out id );
		}

		public void Bind ( ) {
			GL.BindBuffer( Type, id );
		}

		~Buffer ( ) {
			GL.DeleteBuffers( 1, ref id );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glGenBuffers" )]
		public static extern void GenBuffers ( int count, out uint buffer );

		[DllImport( "opengl32.dll", EntryPoint = "glBindBuffer" )]
		public static extern void BindBuffer ( BufferType type, uint buffer );

		[DllImport( "opengl32.dll", EntryPoint = "glDeleteBuffers" )]
		public static extern void DeleteBuffers ( int count, ref uint buffer );
	}
}