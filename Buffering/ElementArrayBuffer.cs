using System;

namespace OpenGL.Buffering {
	public class ElementArrayBuffer: Buffer<uint> {
		public ElementArrayBuffer ( BufferUsage usage )
		:base( BufferType.ElementArrayBuffer, usage ) {
		}
	}
}