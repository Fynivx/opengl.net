using System.Runtime.InteropServices;
using OpenGL.Common;
using OpenGL.Native;

namespace OpenGL.Buffering {
	public class VertexArray: BaseResource {

		public VertexArray ( ) {
			GL.GenVertexArrays( 1, out id );
		}

		public void Bind ( ) {
			GL.BindVertexArray( id );
		}

		~VertexArray ( ) {
			GL.DeleteVertexArrays( 1, ref id );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glGenVertexArrays" )]
		public static extern void GenVertexArrays ( int count, out uint array );

		[DllImport( "opengl32.dll", EntryPoint = "glBindVertexArray" )]
		public static extern void BindVertexArray ( uint array );

		[DllImport( "opengl32.dll", EntryPoint = "glDeleteVertexArrays" )]
		public static extern void DeleteVertexArrays ( int count, ref uint array );
	}
}