using System;

namespace OpenGL.Buffering {
	public class Buffer<TItem>:Buffer where TItem: struct {
		public Buffer ( BufferType type, BufferUsage usage )
		:base( type, usage ) {
		}

		public BufferMap<TItem> Map {
			get {
				return new BufferMap<TItem> ( id );
			}
		}
	}
}