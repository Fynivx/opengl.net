using System;

namespace OpenGL.Buffering {
	public class ArrayBuffer<TItem>: Buffer<TItem> where TItem: struct {
		public ArrayBuffer ( BufferUsage usage )
			:base( BufferType.ArrayBuffer, usage ) {
		}
	}
}

