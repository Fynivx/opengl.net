using System.Runtime.InteropServices;
using OpenGL.Common;
using OpenGL.Native;
using OpenGL.Queries;

namespace OpenGL.Queries {
	public class Query: BaseResource {
		QueryType type;

		public Query ( QueryType type ) {
			this.type = type;
			GL.GenQueries( 1, out id );
		}

		public void Begin ( ) {
			GL.BeginQuery( type, id );
		}

		public void End ( ) {
			GL.EndQuery( type );
		}

		public void Counter ( ) {
			GL.QueryCounter( id, type );
		}

		~Query () {
			GL.DeleteQueries( 1, ref id );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glGenQueries" )]
		public static extern void GenQueries ( int count, out uint query );

		[DllImport( "opengl32.dll", EntryPoint = "glBeginQuery" )]
		public static extern void BeginQuery ( QueryType type, uint query );

		[DllImport( "opengl32.dll", EntryPoint = "glEndQuery" )]
		public static extern void EndQuery ( QueryType type );

		[DllImport( "opengl32.dll", EntryPoint = "glDeleteQueries" )]
		public static extern void DeleteQueries ( int count, ref uint query );

		[DllImport( "opengl32.dll", EntryPoint = "glQueryCounter" )]
		public static extern void QueryCounter ( uint query, QueryType type );
	}
}