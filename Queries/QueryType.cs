namespace OpenGL.Queries {
	public enum QueryType: uint {
		SamplesPassed = 0x8914,
		AnySamplesPassed = 0x8C2F,
		AnySamplesPassedConservative = 0x8D6A,
		PrimitivesGenerated = 0x8C87,
		TransformFeedbackPrimitivesWritten = 0x8C88,
		TimeElapsed = 0x88BF
	}
}