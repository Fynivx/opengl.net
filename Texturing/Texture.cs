using System.Runtime.InteropServices;
using OpenGL.Common;
using OpenGL.Native;

namespace OpenGL.Texturing {
	public class Texture<ColorType, ColorComponent>: BaseResource
		where ColorType: struct
			where ColorComponent: struct {

		public Texture ( ) {
			GL.GenTextures( 1, out id );
		}

		~Texture (  ) {
			GL.DeleteTextures( 1, ref id );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint = "glGenTextures" )]
		public static extern void GenTextures ( int count, out uint texture );

		[DllImport( "opengl32.dll", EntryPoint = "glDeleteTextures" )]
		public static extern void DeleteTextures ( int count, ref uint texture );
	}
}