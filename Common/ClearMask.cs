using System;

namespace OpenGL.Common {
	[Flags]
	public enum ClearMask: uint {
		ColorBufferBit = 0x00004000,
		DepthBufferBit = 0x00000100,
		AccumBufferBit = 0x00000200,
		StencilBufferBit = 0x00000400
	}
}
