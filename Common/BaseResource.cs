// warning: describe Finalize as standard destructor
#pragma warning disable 465
using System;
using System.Runtime.ConstrainedExecution;

namespace OpenGL.Common {
	/*
		Base class for all OpenGL resources.
		Provides a guaranteed cleaning for all child classes.
	*/
	public abstract class BaseResource: CriticalFinalizerObject, IDisposable {
		internal uint id;

		public abstract void Finalize ( );

		public void Dispose ( ) {
			Finalize( );
			GC.SuppressFinalize( this );
		}
	}
}
