using System.Runtime.InteropServices;
using OpenGL.Native;
using OpenGL.Common;

namespace OpenGL.Common {
	public abstract class WindowBase: BaseResource {
		internal abstract void SaveCurrentAndUseThis ( );

		internal abstract void RestorePrevious ( );

		public void SetClearColor ( float r, float g, float b, float a ) {
			SaveCurrentAndUseThis( );
			GL.ClearColor( r, g, b, a );
			RestorePrevious( );
		}

		public void Clear ( ClearMask mask ) {
			SaveCurrentAndUseThis( );
			GL.Clear( mask );
			RestorePrevious( );
		}
	}
}
namespace OpenGL.Native {
	internal static partial class GL {
		[DllImport( "opengl32.dll", EntryPoint="glClearColor" )]
		public static extern void ClearColor ( float r, float g, float b, float a );

		[DllImport( "opengl32.dll", EntryPoint="glClear" )]
		public static extern void Clear ( ClearMask mask );
	}
}
